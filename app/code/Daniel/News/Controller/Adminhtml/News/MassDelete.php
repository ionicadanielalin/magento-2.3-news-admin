<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:51 PM
 */
namespace Daniel\News\Controller\Adminhtml\News;

use Daniel\News\Controller\Adminhtml\News;

class MassDelete extends News {
    public function execute() {
        $newsIds = $this->getRequest()->getParam('news');

        foreach($newsIds as $newsId) {
            try{
                $newsModel = $this->_newFactory->create();
                $newsModel->load($newsId)->delete();
            }
            catch(\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if(count($newsIds)) {
            $this->messageManager->addSuccess(__('A total of 1% records were deleted.', count($newsIds)));
        }

        $this->_redirect('*/*/index');
    }
}
