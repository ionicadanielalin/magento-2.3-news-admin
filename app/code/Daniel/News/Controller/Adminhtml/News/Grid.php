<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:29 PM
 */
namespace Daniel\News\Controller\Adminhtml\News;

use Daniel\News\Controller\Adminhtml\News;

/*
 This is the grid action which is used for loading grid by ajax
*/
class Grid extends News {
    public function execute() {
        return $this->_resultPageFactory->create();
    }
}