<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:47 PM
 */
namespace Daniel\DanielNews\Controller\Adminhtml\News;

use Daniel\News\Controller\Adminhtml\News;

/** This is the edit action for editing news page */
class Edit extends News {
    public function execute() {
        $newsId = $this->getRequest()->getParam('id');
        $model = $this->_newsFactory->create();

        if($newsId) {
            $model->load($newsId); //just load the id to get the object

            if(!$model->getId()) {
                $this->messageManager->addError(__('This news no longer exists.'));
                $this->_redirect('*/*/');

                return;
            }
        }

        $data = $this->_session->getNewsData(true);
        if(!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('news_news', $model);

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Daniel_News::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('News'));

        return $resultPage;
    }
}