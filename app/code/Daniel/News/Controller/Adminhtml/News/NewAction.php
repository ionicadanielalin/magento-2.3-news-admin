<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:45 PM
 */
namespace Daniel\News\Controller\Adminhtml\News;

use Daniel\News\Controller\Adminhtml\News;

class NewAction extends News {
    public function execute() {
        $this->_forward('edit');
    }
}