<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 1:39 AM
 */
namespace Daniel\News\Controller\Index;

use Daniel\News\Model\NewsFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    protected $_modelNewsFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        NewsFactory $modelNewsFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_modelNewsFactory = $modelNewsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        //return $this->resultPageFactory->create();
        $newsModel = $this->_modelNewsFactory->create();

        $item = $newsModel->load(1);
        var_dump($item->getData());

        $newsCollection = $newsModel->getCollection();
        var_dump($newsCollection->getData());
    }
}