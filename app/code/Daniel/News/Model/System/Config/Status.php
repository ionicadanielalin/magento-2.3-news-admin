<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:19 PM
 */
namespace Daniel\News\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface {
    const ENABLED = 1;
    const DISABLED = 0;

    public function toOptionArray() {
        $options = [
            self::ENABLED => __('Enabled'),
            self::DISABLED => __('Disabled')
        ];

        return $options;
    }
}