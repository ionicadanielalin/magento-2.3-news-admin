<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 1:45 AM
 */
namespace Daniel\News\Model\System\Config\LatestNews;

use Magento\Framework\Option\ArrayInterface;

class Position implements ArrayInterface {
    const LEFT = 1;
    const RIGHT = 2;
    const DISABLED = 0;

    public function toOptionArray() {
        return [
            self::LEFT => __("Left"),
            self::RIGHT => __('Right'),
            self::DISABLED => __('Disabled')
        ];
    }
}