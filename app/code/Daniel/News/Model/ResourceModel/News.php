<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 1:31 AM
 */
namespace Daniel\News\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class News extends AbstractDb {
    protected function _construct() {
        $this->_init('tutorial_simplenews', 'id');
    }
}