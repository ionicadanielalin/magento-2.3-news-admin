<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 1:30 AM
 */
namespace Daniel\News\Model;

use Magento\Framework\Model\AbstractModel;

class News extends AbstractModel {
    protected function _construct() {
        /** @var resourceModel classname */
        $this->_init('Daniel\News\Model\ResourceModel\News');
    }
}