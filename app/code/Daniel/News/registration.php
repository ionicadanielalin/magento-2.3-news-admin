<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 1:24 AM
 */
\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Daniel_News',
        __DIR__
    );