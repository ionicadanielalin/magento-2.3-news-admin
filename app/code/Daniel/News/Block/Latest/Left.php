<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 2:22 AM
 */
namespace Daniel\News\Block\Latest;

use Daniel\News\Block\Latest;
use Daniel\News\Model\System\Config\LatestNews\Position;

class Left extends Latest {
    public function _construct() {
        $position = $this->_dataHelper->getLatestNewsBlockPosition();

        if($position == Position::LEFT) {
            $this->setTemplate('Jeff_SimpleNews::latest.phtml');
        }
    }
}