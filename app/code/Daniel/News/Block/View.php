<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 2:14 AM
 */
namespace Daniel\News\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;

class View extends Template {
    protected $_coreRegistry;

    public function __construct( Template\Context $context, Registry $coreRegistry, array $data =[]) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function getNewsInformation() {
        return $this->_coreRegistry->registry('newsData');
    }
}