<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/29/2019
 * Time: 4:20 PM
 */
namespace Daniel\News\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class News extends Container {
    protected function _construct() {
        $this->_controller = 'adminhtml_news'; //controller name
        $this->_blockGroup = 'Daniel_News'; //Module name
        $this->_headerText = __('Manage News');
        $this->_addButtonLabel = __('Add News');
        parent::_construct();
    }
}