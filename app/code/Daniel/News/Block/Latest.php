<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 10/24/2019
 * Time: 2:20 AM
 */
namespace Daniel\News\Block;

use Magento\Framework\View\Element\Template;
use Daniel\News\Helper\Data;
use Daniel\News\Model\NewsFactory;
use Daniel\News\Model\System\Config\Status;

class Latest extends Template {
    protected $_dataHelper;
    protected $_newsFactory;

    public function __construct(Template\Context $context, Data $dataHelper, NewsFactory $newsFactory) {
        $this->_dataHelper = $dataHelper;
        $this->_newsFactory = $newsFactory;
        parent::__construct($context);
    }

    //Get five latest news
    public function getLatestNews() {
        $collection = $this->_newsFactory->create()->getCollection();
        $collection->addFieldToFilter('status', ['eq'=> 1]);
        $collection->getSelect()->order('id DESC')->limit(5);

        return $collection;
    }
}